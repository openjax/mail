# OpenJAX Mail

> Java API Extensions to `javax.mail`

[![Build Status](https://travis-ci.org/openjax/mail.png)](https://travis-ci.org/openjax/mail)
[![Coverage Status](https://coveralls.io/repos/github/openjax/mail/badge.svg?branch=master)](https://coveralls.io/github/openjax/mail?branch=master)

### JavaDocs

JavaDocs are available [here](https://ext.openjax.org/mail/apidocs/).

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

### License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.